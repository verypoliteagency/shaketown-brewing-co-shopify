//////////////////////////////////////////////////////////
////  Stockists
//////////////////////////////////////////////////////////

const Stockists = (() => {

  let debug = false;
  let info = { name : 'Stockists', version : '1.0' };

  let tools = new Tools();
  let breakpoints = new Breakpoints();

  let sheetDBEndpoint = 'https://sheetdb.io/api/v1/dl5b448xyfbad';
  let targetElement = document.querySelector('.stockists__main') || false;
  let includeDirectionsLink = false;
  let resetLocalStorage = false;
  let minutesBeforeRequestAPIAgain = 360;
  let stockists = {
    lists: '',
    groups: '',
    storageDate: '',
    storageKey: 'SHAKE--stockists',
   };

  //////////////////////////////////////////////////////////
  ////  Get Stockists
  //////////////////////////////////////////////////////////

  const getStockistsChunks = ( $items = [], $groups = [] ) => {

    let chunks = [];
    let items = [].concat(...$items);

    $groups.forEach( group => {

      let stockistGroup = {
        group: group,
        stockists: [],
      };

      $items.forEach( item => {
        if ( item.group === group && !item.hidden ) {
          stockistGroup.stockists.push( item );
        }
      });

      if ( stockistGroup.stockists.length ) {
        chunks.push( stockistGroup );
      }

    });

    return chunks;

  }

  //////////////////////////////////////////////////////////
  ////  Chunk
  //////////////////////////////////////////////////////////

  const getStockists = () => {

    if ( targetElement ) {

      resetLocalStorage = targetElement.dataset.clearLocalStorage === "true" ? true : false;
      includeDirectionsLink = targetElement.dataset.includeDirectionsLink === "true" ? true : false;

      if ( resetLocalStorage ) {
        localStorage.removeItem( stockists.localStorageKey );
      }

      if ( localStorage.getItem( stockists.localStorageKey ) ) {

        let storedData = JSON.parse( localStorage.getItem( stockists.localStorageKey ) );
        let millisecondsDifference = Date.now() - storedData.storageDate;
        let minutesDifference = ( millisecondsDifference / 60000 ).toFixed(2);

        if ( minutesDifference > minutesBeforeRequestAPIAgain ) {
          getStockistsFromAPI();
        } else {
          printStockists( stockists );
        }

      } else {
        getStockistsFromAPI();
      }
    }

  };

  //////////////////////////////////////////////////////////
  ////  Get Stockists from API
  //////////////////////////////////////////////////////////

  const getStockistsFromAPI = () => {

    SheetDB.read( sheetDBEndpoint, {} ).then(function( result ){

      result = result.sort( (a, b) => ( a.name > b.name ) ? 1 : -1 );
      stockists.groups = getSortedStockistsGroups( result );
      stockists.lists = getStockistsChunks( result, stockists.groups );
      stockists.storageDate = Date.now();

      printStockists( stockists );
      localStorage.setItem( stockists.storageKey, JSON.stringify( stockists ) );

    }, function( error ) {
      if ( debug ) console.log( error );
    });

  };

  //////////////////////////////////////////////////////////
  ////  Get Stockists Groups
  //////////////////////////////////////////////////////////

  const getSortedStockistsGroups = ( $stockists = [] ) => {

    let groups = $stockists.map( stockist => {
      return stockist.group;
    });

    return [ ...new Set( groups ) ].sort();

  };

  //////////////////////////////////////////////////////////
  ////  Print Stockists Groups
  //////////////////////////////////////////////////////////

  const printStockists = ( $stockists = {} ) => {

    if ( debug ) console.log( 'printStockists', $stockists );

    let html = '';
    let { lists = [] } = $stockists;

    if ( lists.length ) {
      lists.forEach( ( list, index ) => {
        if ( list.stockists.length ) {
          html += '<div class="stockists__group" data-count="' + index + '">';
            html += '<h2 class="stockists__heading heading"><span>' + list.group + '</span></h2>';
            html += printStockistsList( list.stockists );
          html += '</div>';
        }
      });
      targetElement.innerHTML = html;
    }

  };

  //////////////////////////////////////////////////////////
  ////  Print Stockists List
  //////////////////////////////////////////////////////////

  const printStockistsList = ( $list = [] ) => {

    let html = '';

    if ( $list.length ) {
      html += '<div class="stockists__grid b-4 b-solid b-black round-8">';
        $list.forEach( stockistItem => {
          html += printStockistsItem( stockistItem );
        });
      html += '</div>';
    }

    return html;

  };

  //////////////////////////////////////////////////////////
  ////  Print Stockists Item
  //////////////////////////////////////////////////////////

  const printStockistsItem = ( $stockistItem = false ) => {

    let html = '';
    let {
      name = false,
      address = false,
      address2 = false,
      city = false,
      region = false,
      postal = false,
      country = false,
    } = $stockistItem;
    let directionsLink = includeDirectionsLink ? getDirectionsLink( $stockistItem ) : '';

    html += '<div class="stockists__item">';

      html += name ? '<h3 class="stockists__name">' + name + '</h3>' : '';

      if ( address || city ) {

        html += '<p class="stockists__address">';

          html += directionsLink ? '<a class="stockists__link" href="' + directionsLink + '" target="_blank" rel="noopener">' : '';

          // 680–1236 Main St
          if ( address ) {
            html += '<span>';
            if ( address2 ) {
              html += address2 + "–" + address;
            } else {
              html += address;
            }
            html += '</span>';
          }

          // Vancouver, BC V6K 3Y2
          if ( city ) {
            html += '<span>';
              html += city;
              html += region ? ', ' + region : '';
              html += postal ? ' ' + postal : '';
            html += '</span>';
          }

          html += directionsLink ? '</a>' : '';

        html += '</p>';

      }

    html += '</div>';

    return html;

  };

  //////////////////////////////////////////////////////////
  ////  Get Directions Link from Stockist Object
  //////////////////////////////////////////////////////////

  const getDirectionsLink = ( $stockist = {} ) => {

    let baseURL = 'https://www.google.com/maps/search/?api=1&query=';
    let query = '';

    let {
      name = false,
      address = false,
      address2 = false,
      city = false,
      region = false,
      postal = false,
      country = false,
    } = $stockist;

    // 1200 Pennsylvania Ave SE, Washington, District of Columbia, 20003 + Business Name
    query += address2 ? address2 + '–' : '';
    query += address ? address : '';
    query += city ? ', ' + city : '';
    query += region ? ', ' + region : '';
    query += postal ? ', ' + postal : '';
    query += country ? ', ' + country : '';
    query += name ? ' + ' + name : '';

    if ( query ) {
      return baseURL + encodeURIComponent( query.trim() );
    }

    return false;

  };

  //////////////////////////////////////////////////////////
  ////  Public Method | Initialize
  //////////////////////////////////////////////////////////

  const init = () => {
    if ( debug ) console.log( `${info.name}.init() Started` );
    getStockists();
    if ( debug ) console.log( `${info.name}.init() Finished` );
  };

  //////////////////////////////////////////////////////////
  ////  Returned Methods
  //////////////////////////////////////////////////////////

  return {
    debug,
    info,
    init
  };

});
