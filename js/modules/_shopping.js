//////////////////////////////////////////////////////////
////  Shopping
//////////////////////////////////////////////////////////

const Shopping = (() => {

  const debug = false;
  const info = { name : 'Shopping', version : '1.0' };

  //////////////////////////////////////////////////////////
  ////  Update Product Quantity
  //////////////////////////////////////////////////////////

  function updateProductQty(){

    document.querySelectorAll('.button--quantity').forEach((button) => {
      button.addEventListener( 'click', function( event ) {
        updateQuantityByButton( button );
      });
    });

    // ---------------------------------------- Update product quantity based on button class, either 'increase' or 'decrease'
    function updateQuantityByButton( $target = false ) {

      let product = $target.closest('.product') || false;
      let productQtyInput = product.querySelector('.product__quantity-input') || false;
      let productQtyTotal = product.querySelector('.product__quantity-total') || false;

      if ( $target.classList.contains('increase') ) {
        productQtyInput.value = ++productQtyInput.value;
      } else {
        if ( productQtyInput.value > 1 ) {
          productQtyInput.value = --productQtyInput.value;
        }
      }

      productQtyTotal.innerHTML = productQtyInput.value;

    }

  };

  //////////////////////////////////////////////////////////
  ////  Init
  //////////////////////////////////////////////////////////

  const init = ( $options = false ) => {

    if ( debug ) console.log( `${info.name}.init() Started` );
    updateProductQty();
    if ( debug ) console.log( `${info.name}.init() Finished` );

  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    debug,
    info,
    init
  };

});
