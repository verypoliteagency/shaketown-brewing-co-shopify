//////////////////////////////////////////////////////////
////  Shopping
//////////////////////////////////////////////////////////

const GoogleMaps = (() => {

  const debug = false;
  const info = { name : 'GoogleMaps', version : '1.0' };

  let queryTargetElement = '.js--google-map';

  //////////////////////////////////////////////////////////
  ////  Get Google Maps
  //////////////////////////////////////////////////////////

  const getGoogleMaps = () => {
    return document.querySelectorAll( queryTargetElement ) || [];
  };

  //////////////////////////////////////////////////////////
  ////  Init
  //////////////////////////////////////////////////////////

  const init = ( $options = false ) => {

    if ( debug ) console.log( `${info.name}.init() Started` );

    let maps = getGoogleMaps();

    if ( maps ) {

      const loader = new google.maps.plugins.loader.Loader({
        apiKey: "AIzaSyAKcmVh7RVnOoNL3tpG8GNCfvVzXGlmW3s",
        version: "weekly",
      });

      for ( let i = 0; i < maps.length; i++ ) {

        let map, geocoder;
        let address = maps[i].dataset.address || 'Vancouver, BC, Canada';
        let zoom = parseInt(maps[i].dataset.zoom) || 12;

        loader.load().then(() => {

          geocoder = new google.maps.Geocoder();

          map = new google.maps.Map( maps[i], {
            center: {lat: -34.397, lng: 150.644},
            zoom: zoom
          });

          geocoder.geocode( { 'address': address }, function(results, status) {
            if (status == 'OK') {
              map.setCenter(results[0].geometry.location);
              var marker = new google.maps.Marker({
                  map: map,
                  position: results[0].geometry.location
              });
            } else {
              console.log('Geocode was not successful for the following reason: ' + status);
            }
          });

        });

      }

    } else {
      console.log( `${info.name} :: No Maps!` );
    }

    if ( debug ) console.log( `${info.name}.init() Finished` );

  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    debug,
    info,
    init
  };

});
