//////////////////////////////////////////////////////////
////  Modals
//////////////////////////////////////////////////////////

const Modals = (() => {

  let debug = false;
  let info = { name : 'Modals', version : '2.5' };

  //////////////////////////////////////////////////////////
  ////  On click, close modal
  //////////////////////////////////////////////////////////

  const onClickCloseModal = () => {
    document.querySelectorAll( '.button[data-micromodal-close]' ).forEach( button => {
      button.addEventListener( 'click', ( event ) => {
        let microModal = button.closest('.modal') || false;
        let microModalID = microModal.id;
        if ( microModalID ) toggleModalVisibility( microModalID, 'close' );
      });
    });
  };

  //////////////////////////////////////////////////////////
  ////  On click, open modal
  //////////////////////////////////////////////////////////

  const onClickOpenModal = () => {
    document.querySelectorAll( '.button[data-micromodal-open]' ).forEach( button => {
      button.addEventListener( 'click', ( event ) => {
        let microModalID = button.dataset.modalID || false;
        if ( microModalID ) toggleModalVisibility( microModalID, 'open' );
      });
    });
  };

  //////////////////////////////////////////////////////////
  ////  Toggle Modal Visibility
  //////////////////////////////////////////////////////////

  const toggleModalVisibility = ( $targetElementID = '', $method = 'show', $options = {}, $delay = 0 ) => {

    let modal = document.getElementById( $targetElementID ) || false;
    let acceptedMethods = [ 'show', 'close' ].includes( $method );
    let options = {
      onShow: function( modal ) {},
      onClose: function( modal ) {},
      awaitCloseAnimation: true
    };

    if ( modal && acceptedMethods ) {

      if ( $options && (typeof $options == "object") ) {
        options = { ...options, ...$options };
      }

      setTimeout(function(){
        MicroModal[$method]( $targetElementID, options );
      }, $delay );

    }

  };

  //////////////////////////////////////////////////////////
  ////  Returned
  //////////////////////////////////////////////////////////

  return {
    debug,
    info,
    onClickCloseModal,
    onClickOpenModal,
    toggleModalVisibility
  };

});
