//////////////////////////////////////////////////////////
////  Scrolling
//////////////////////////////////////////////////////////

const Scrolling = (() => {

	let debugThis = false;
	let info = { name : 'Scrolling', version : '1.0' };
	let count = 0;
	let scrollPostLast = 0;

	//////////////////////////////////////////////////////////
  ////  Scroll Status
  //////////////////////////////////////////////////////////

  const scrollStatus = () => {

    let targets = 'body, header, [role="main"]';

    let scrolling = false;
    let atTop = true;
    let scrollDirectionDown = true;

    let scrollPosTop = 0;
    let scrollPosInit = window.pageYOffset || document.documentElement.scrollTop;

    setStatus();

		Browser.onScroll( setStatus, 50 );

    function setStatus() {

      // get curren pos
      let scrollPosCurrent = window.pageYOffset || document.documentElement.scrollTop; // Browser fallback

      // compare current position to top position
      scrolling = scrollPosCurrent > scrollPosTop;
      atTop = scrollPosCurrent == scrollPosTop;

      // compare current to last to see which direction scrolling
      scrollDirectionDown = ( scrollPosCurrent > scrollPostLast ) ? true : false;

      // set last scroll position
      scrollPostLast = ( scrollPosCurrent <= 0 ) ? 0 : scrollPosCurrent;

      // add classes
      if ( scrolling ) {
        $( targets ).addClass('scroll-pos--scrolled');
        $( targets ).removeClass('scroll-pos--at-top');

      } else {
        $( targets ).addClass('scroll-pos--at-top');
        $( targets ).removeClass('scroll-pos--scrolled');
      }

      if ( scrollDirectionDown ) {
        $( targets ).addClass('scroll-dir--down');
        $( targets ).removeClass('scroll-dir--up');
      } else {
        $( targets ).addClass('scroll-dir--up');
        $( targets ).removeClass('scroll-dir--down');
      }

      if ( debugThis ) {
        console.log('[ scrollStatus() ]');
        console.log({
          'at-top' : atTop,
          'scrolling' : scrolling,
          'direction-down' : scrollDirectionDown,
          'pos-last' : scrollPostLast,
          'pos-current' : scrollPosCurrent
        });
      }

    }

  };

	//////////////////////////////////////////////////////////
	//// Public
	//////////////////////////////////////////////////////////

	const init = () => {
		scrollStatus();
	};

	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////

	return {
		init : init
	};

})();
