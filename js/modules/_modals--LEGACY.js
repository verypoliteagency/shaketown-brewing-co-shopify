//////////////////////////////////////////////////////////
////  Modals
//////////////////////////////////////////////////////////

const Modals = (() => {

	let DEBUG = false;
	let INFO = { name : 'Modals', version : '1.0' };

	//////////////////////////////////////////////////////////
  ////  Newsletter
  //////////////////////////////////////////////////////////

	const newsletter = {

  	cookie: {
    	name: "newsletter",
      value: "seen",
      expires: 180
  	},

  	target: "newsletter",

  	setCookie: function() {
    	Browser.setCookie( this.cookie.name, this.cookie.value, this.cookie.expires );
    },

    show: function( $delay ) {

      let target = this.target;
      let onClose = this.setCookie();

      setTimeout(function(){
        MicroModal.show( target, {
          awaitCloseAnimation: true,
          onShow: function(modal) {
            console.log("micromodal opened");
          },
          onClose: function(modal) {
            console.log("micromodal closed");
            onClose;
          }
        });
      }, $delay );

    },

    init: function() {

      let cookieNotSeen = Browser.getCookie( this.cookie.name ) !== "seen";
      let modalExists = $("#" + this.target).length;

      if ( modalExists ) {
  		  this.show( 1500 );
  		}

  		$("#" + this.target + " [data-micromodal-close]").on("click", function(){
        MicroModal.close( this.target );
  		});

    },

	};

	//////////////////////////////////////////////////////////
  ////  Share
  //////////////////////////////////////////////////////////

	const share = {

  	target: 'share',

    init: function() {

      let modalExists = $("#" + this.target).length;
      let target = this.target;

      if ( modalExists ) {

        $(".js-trigger--share").on("click", function(){

          MicroModal.show( target, {
            awaitCloseAnimation: true,
            onShow: function(modal) {
              console.log("micromodal opened");
            },
            onClose: function(modal) {
              console.log("micromodal closed");
            }
          });

        });

  		  $("#" + this.target + " [data-micromodal-close]").on("click", function(){
          MicroModal.close( target );
    		});

  		}

    },

	};

	//////////////////////////////////////////////////////////
  ////  Main
  //////////////////////////////////////////////////////////

  const main = () => {

    // newsletter.init();

    // share.init();

  };

	//////////////////////////////////////////////////////////
	//// Init
	//////////////////////////////////////////////////////////

	const init = () => {
		main();
	};

	//////////////////////////////////////////////////////////
	////  Returned
	//////////////////////////////////////////////////////////

	return {
    init : init
	};

})();
