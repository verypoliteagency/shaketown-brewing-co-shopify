///////////////////////////////////////////////////////////////////////////////////////////////
////  Vendor
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend quiet "../node_modules/axios/dist/axios.min.js";
// @codekit-prepend quiet "../node_modules/bootstrap/dist/js/bootstrap.bundle.min.js";
// @codekit-prepend quiet "../node_modules/@glidejs/glide/dist/glide.min.js";
// @codekit-prepend quiet "../node_modules/validator/validator.min.js";
// @codekit-prepend quiet "../node_modules/micromodal/dist/micromodal.min.js";
// @codekit-prepend quiet "../node_modules/animejs/lib/anime.min.js";
// @codekit-prepend quiet "../node_modules/@googlemaps/js-api-loader/dist/index.umd.js";
// @codekit-prepend quiet "../node_modules/sheetdb-js/build/sheetdb-js.js";

///////////////////////////////////////////////////////////////////////////////////////////////
////  Polite Department
///////////////////////////////////////////////////////////////////////////////////////////////

// @codekit-prepend "./modules/_ageGate.js";
// @codekit-prepend "./modules/_breakpoints.js";
// @codekit-prepend "./modules/_credits.js";
// @codekit-prepend "./modules/_forms.js";
// @codekit-prepend "./modules/_gliders.js";
// @codekit-prepend "./modules/_googleMaps.js";
// @codekit-prepend "./modules/_instagramFeed.js";
// @codekit-prepend "./modules/_mobileMenu.js";
// @codekit-prepend "./modules/_modals.js";
// @codekit-prepend "./modules/_shopping.js";
// @codekit-prepend "./modules/_stockists.js";
// @codekit-prepend "./modules/_theme.js";
// @codekit-prepend "./modules/_tools.js";

let credits = new Credits();
let ageGate = new AgeGate();
let forms = new Forms();
let gliders = new Gliders();
let googleMaps = new GoogleMaps();
let instagramFeed = new InstagramFeed();
let mobileMenu = new MobileMenu();
let shopping = new Shopping();
let stockists = new Stockists();

Theme.init([
  credits,
  ageGate,
  forms,
  gliders,
  googleMaps,
  instagramFeed,
  mobileMenu,
  shopping,
  stockists
]);

